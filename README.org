#+TITLE: Spritely Crystal

Spritely Crystal is an implementation and demo of how to bring
privacy-preserving, persistent URLs to the web which are capable
of being updated.

For more information, read [[file:./crystal/scribblings/intro.org][the introduction]].
